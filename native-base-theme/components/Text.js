import variable from './../variables/platform';
import {Fonts} from '/Users/meharnallamalli/Desktop/DinePro/src/native/constants/Fonts';

export default (variables = variable) => {
  const textTheme = {
    fontSize: variables.DefaultFontSize - 1,
    fontFamily: variables.fontFamily,
    color: variables.textColor,
    '.note': {
      color: '#a7a7a7',
      fontSize: variables.noteFontSize,
    },
  };

  return textTheme;
};
