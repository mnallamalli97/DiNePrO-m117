import Store from '../store/ucla';

export const initialState = Store;

export default function uclaReducer(state = initialState, action) {
  switch (action.type) {
    case 'UCLA_FAVOURITES_REPLACE': {
      return {
        ...state,
        favourites: action.data || [],
      };
    }
    case 'UCLA_MEALS_REPLACE': {
      return {
        ...state,
        error: null,
        loading: false,
        meals: action.data,
      };
    }
    case 'UCLA_ERROR': {
      return {
        ...state,
        error: action.data,
      };
    }
    case 'UCLA_REPLACE': {
      let ucla = [];

      // Pick out the props I need
      if (action.data && typeof action.data === 'object') {
        ucla = action.data.map(item => ({
          id: item.id,
          title: item.title,
          body: item.body,
          category: item.category,
          image: item.image,
          author: item.author,
          hours: item.hours,
          address: item.address,
          activityInd: item.activityInd,
          menu: item.menu,
          method: item.method,
        }));
      }

      return {
        ...state,
        error: null,
        loading: false,
        ucla,
      };
    }
    default:
      return state;
  }
}
