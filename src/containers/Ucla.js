import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getUCLARecipes, getUCLAMeals, setUCLAError } from '../actions/ucla';

class UCLARecipeListing extends Component {
  static propTypes = {
    Layout: PropTypes.func.isRequired,
    ucla: PropTypes.shape({
      loading: PropTypes.bool.isRequired,
      error: PropTypes.string,
      ucla: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    }).isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({}),
    }),
    getUCLARecipes: PropTypes.func.isRequired,
    getUCLAMeals: PropTypes.func.isRequired,
    setUCLAError: PropTypes.func.isRequired,
  }

  static defaultProps = {
    match: null,
  }

  componentDidMount = () => this.fetchUCLARecipes();

  /**
    * Fetch Data from API, saving to Redux
    */
  fetchUCLARecipes = () => {
    return this.props.getUCLARecipes()
      .then(() => this.props.getUCLAMeals())
      .catch((err) => {
        console.log(`Error: ${err}`);
        return this.props.setUCLAError(err);
      });
  }

  render = () => {
    const { Layout, ucla, match } = this.props;
    const id = (match && match.params && match.params.id) ? match.params.id : null;

    return (
      <Layout
        recipeId={id}
        error={ucla.error}
        loading={ucla.loading}
        ucla={ucla.ucla}
        reFetch={() => this.fetchUCLARecipes()}
      />
    );
  }
}

const mapStateToProps = state => ({
  ucla: state.ucla || {},
});

const mapDispatchToProps = {
  getUCLARecipes,
  getUCLAMeals,
  setUCLAError,
};

export default connect(mapStateToProps, mapDispatchToProps)(UCLARecipeListing);
