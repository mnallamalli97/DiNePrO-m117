import React from 'react';
import PropTypes from 'prop-types';
import { Font } from 'expo';
import { FlatList, TouchableOpacity, TouchableHighlight, RefreshControl, Image } from 'react-native';
import { Container, Content, Card, CardItem, Body, Text, Button, StyleSheet, View, H1, H2, H3 } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Loading from './Loading';
import Error from './Error';
import Header from './Header';
import Spacer from './Spacer';
import MapView from 'react-native-maps';
import Search from 'react-native-search-box';
import SearchBar from 'react-native-material-design-searchbar'
import * as Progress from 'react-native-progress';
import { Fonts } from '/Users/meharnallamalli/Desktop/DinePro/src/native/constants/Fonts';

import getLocation from '../../util/getLocation';
import getDay from '../../util/getDay';

const fontFamily = {
  Elianto: "Elianto",
}
const keyExtractor = item => item.id;
const onPressBusyness = item => Actions.recipe({ match: { params: { id: String(item.id) } } });
const onPressMenu = item => Actions.menu({ match: { params: { id: String(item.id) } } });


const Restaurants = ({
  error,
  loading,
  recipes,
  reFetch,
}) => {
  // Loading
  if (loading) return <Loading />;
  if (error) return <Error content={error} />;


  // Error
  

  
  return (
    <Container>

     <SearchBar
        onSearchChange={() => console.log('On Search Change')}
        height={50}
        onFocus={() => console.log('On Focus')}
        onBlur={() => console.log('On Blur')}
        placeholder={'Search...'}
        autoCorrect={false}
        padding={5}
        returnKeyType={'search'}
      />
        
      <Content padder>
        <Header style={{marginBottom: 100, fontFamily: 'Elianto'}}
          title="Restaurants Westwood"
          content="Linked to firebase backend."
        />


        <MapView style={{height:250, width: null, flex:1, borderRadius: 10}}
            scrollEnabled={false}

            initialRegion={{
              latitude: 34.061695,
              longitude: -118.445092,
              latitudeDelta: 0.0042,
              longitudeDelta: 0.0024,
            }}>


          <MapView.Marker
              coordinate={{
                latitude: 34.062793,
                longitude: -118.448087,
              }}
             
          />

          <MapView.Marker
              coordinate={{
                latitude: 34.060927,
                longitude: -118.447478,
              }}
          />

          <MapView.Marker
              coordinate={{
                latitude: 34.063071,
                longitude: -118.448026,
              }}
          />  

          <MapView.Marker
              coordinate={{
                latitude: 34.062580,
                longitude: -118.448037,
              }}
          />

          <MapView.Marker
              coordinate={{
                latitude: 34.062529,
                longitude: -118.448030,
              }}
          />

          <MapView.Marker
              coordinate={{
                latitude: 34.061131,
                longitude: -118.446201,
              }}
          />

          <MapView.Marker
              coordinate={{
                latitude: 34.061111,
                longitude: -118.446132,
              }}
          />


          <MapView.Marker
              coordinate={{
                latitude: 34.062223,
                longitude: -118.446660,
              }}
          />

          <MapView.Marker
              coordinate={{
                latitude: 34.060166,
                longitude: -118.446903,
              }}
          />

          <MapView.Marker
              coordinate={{
                latitude: 34.061076,
                longitude: -118.444649,
              }}
          />
        </MapView>

        <FlatList
          numColumns={1}
          data={recipes}
          renderItem={({ item }) => (
          <Card transparent style={{ paddingHorizontal: 0 }}>
            <CardItem cardBody>
                <Image
                    source={{ uri: item.image }} 
                    style={{
                      justifyContent:'center',
                      alignItems:'center',
                      height: 220,
                      width: null,
                      flex: 1,
                      borderRadius: 10,
                      backgroundColor:'rgba(47, 163, 218, 0.4)' 
                    }} 
                  >

                  <View style={{borderTopWidth:60, borderBottomWidth:15, borderColor: 'transparent'}}>
                    <Text style={{fontSize: 30, color:'white', textAlign:'center', justifyContent:'center', }}> {item.title} </Text>
                    <Text style={{fontSize: 20, color:'white', textAlign:'center', justifyContent:'center',}}> {item.address} </Text>
                    <Text style={{fontSize: 30, color:'white', textAlign:'center', justifyContent:'center',}}>{item.hours}</Text>
                  </View>
                 
                  
                  <Spacer size={5} />


                  <View>
                    <H2 style={{fontSize: 30, color:'white', borderBottomWidth:5, textAlign:'center', justifyContent:'center', }}>Activity Level: 10% </H2>
                    <Progress.Bar progress={10/100} width={300} />
                  </View>


                  <View style={{flex: 1,flexDirection: 'row',alignItems: 'center', justifyContent:'space-between', padding:25}}>
                    <View style={{flex: 1, justifyContent:'space-between', padding:10}}>
                      <Button block bordered small onPress={() => onPressMenu(item)} style={{borderColor:'white'}} > 
                        <Text style={{color:'white'}}>Menu</Text>
                      </Button>
                    </View>
                     
                    <View style={{flex: 1, }}>
                      <Button block bordered small onPress={() => onPressBusyness(item)} style={{borderColor:'white'}}>
                        <Text style={{color:'white'}}>Busyness</Text>
                      </Button>
                    </View>
                  </View>
                </Image>
            </CardItem>
          </Card>
          )}
          keyExtractor={keyExtractor}
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={reFetch}
            />
          }
        />

        <Spacer size={20} />
      </Content>
    </Container>
  );
};

Restaurants.propTypes = {
  error: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  recipes: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  reFetch: PropTypes.func,
};

Restaurants.defaultProps = {
  error: null,
  reFetch: null,
};




export default Restaurants;
