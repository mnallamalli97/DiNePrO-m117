import React, {Component }from 'react';
import PropTypes from 'prop-types';
import { Image, StatusBar } from 'react-native';
import { Container, Content, Card, CardItem, View, Button, Body, H3, List, ListItem, Text, H2} from 'native-base';
import ErrorMessages from '../../constants/errors';
import Error from './Error';
import { Actions } from 'react-native-router-flux';
import Spacer from './Spacer';
import * as Progress from 'react-native-progress';

import getLocation from '../../util/getLocation';
import getDay from '../../util/getDay';

class RecipeView extends Component {

  constructor(props) {
    console.log("COnsturoleskjfsl");
    super(props);

    this.state = {activityLevel : 0,};
    
    let restaurant;
    const { recipes, recipe, recipeId } = props;

    if (recipeId && recipes) {
      restaurant = recipes.find(item => parseInt(item.id, 10) === parseInt(recipeId, 10));
    }

    const restaurantLoc = getLocation(restaurant.id);
    var today = new Date();
    var date = today.toLocaleDateString();
    var dayNum = today.getDay();
    var hour = today.getHours();
   

    const dayOfWeek = getDay(dayNum);

    const fetchURL = 'https://dinepro-backend.herokuapp.com/poptimes/?location=' + restaurantLoc + '&day=' + dayOfWeek + '&time=' + hour;
    console.log(fetchURL);
    const response = fetch(fetchURL)
      .then((response) => response.text())
      .then((text) => {
        // Your code for handling the data you get from the API
        console.log(`value: ${text}`);
        this.setState({
          activityLevel: text,
          restaurant
        })
      })
      .catch(function() {
        // This is where you run code if the server returns any errors
      });
  }

 getBusynessNumber = (hour) => {
  console.log('in getBusynessNumber');
    const restaurantLoc = getLocation(restaurant.id);
    
    var today = new Date();
    var dayNum = today.getDay();
    if (!hour) {
      hour = today.getHours();
    }
    var date = today.toLocaleDateString();

    const dayOfWeek = getDay(dayNum);

    const response = fetch('https://dinepro-backend.herokuapp.com/poptimes/?location=' + restaurantLoc + '&day=' + dayOfWeek + '&time=' + hour);
    const text = response.text();

    this.setState({
      activityLevel: text,
      date,
    })
  }

  onPressBreakfast = () => {
    console.log('SEFSF');
    this.getBusynessNumber(8);
  }

  onPressLunch = () => {
    this.getBusynessNumber(12);
    
  }

  onPressDinner = () => {
    this.getBusynessNumber(19);
   
  }

  onPressCurrent = () => {
    this.getBusynessNumber();
  }

  keyExtractor = item => item.id
  onPressBusyness = item => Actions.recipe({ match: { params: { id: String(item.id) } } })
  onPressMenu = item => Actions.menu({ match: { params: { id: String(item.id) } } })

  render() {

    const {error,
          recipes,
          recipeId
        } = this.props;
      
    const { activityLevel, restaurant, date } = this.state;

    if (error) return <Error content={error} />;

    // Get this Recipe from all recipes

    // restaurant not found
    if (!restaurant) return <Error content={ErrorMessages.recipe404} />;

    // Build Ingredients listing
    const menu = restaurant.menu.map(item => (
      <ListItem key={item} rightIcon={{ style: { opacity: 0 } }}>
        <Text>{item}</Text>
      </ListItem>
    ));

    // Build Method listing
    const method = restaurant.method.map(item => (
      <ListItem key={item} rightIcon={{ style: { opacity: 0 } }}>
        <Text>{item}</Text>
      </ListItem>
    ));

    return (
      <Container>
        <Content padder>
          <Image source={{ uri: restaurant.image }} style={{ height: 150, width: null, flex: 1, borderRadius:10 }} />

          <Spacer size={25} />
          <H3>{restaurant.title}</H3>
          <Text>{restaurant.address}</Text>
          <Spacer size={15} />

          <Card>
            <CardItem header bordered>
              <Text>About this restaurant</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>{restaurant.body}</Text>
              </Body>
            </CardItem>
          </Card>

          <Card>
            <CardItem header bordered>
              <Text>Activity Indicator: {date} </Text>
            </CardItem>
            <CardItem>

              
                    <View style={{flex: 1,flexDirection: 'row',alignItems: 'center', justifyContent:'space-between', padding:6}}>
                      
                      <View style={{flex: 1, justifyContent:'space-between', padding:3}}>
                        <Button block bordered small onPress={() => this.onPressBreakfast()}> 
                          <Text>Breakfast</Text>
                        </Button>
                      </View>
                       
                      <View style={{flex: 1, justifyContent:'space-between', padding:3}}>
                        <Button block bordered small onPress={() => this.onPressLunch()}>
                          <Text>Lunch</Text>
                        </Button>
                      </View>

                      <View style={{flex: 1, justifyContent:'space-between', padding:3 }}>
                        <Button block bordered small onPress={() => this.onPressDinner()}>
                          <Text>Dinner</Text>
                        </Button>
                      </View>

                    </View>      
            </CardItem>

            <CardItem style={{justifyContent:'center', alignItems:'center'}}>
              <Button block bordered small onPress={() => this.onPressCurrent()}>
                 <Text>Munchies right now.</Text>
              </Button>
            </CardItem>

            <CardItem style={{justifyContent:'center', alignItems:'center'}}>
              <Text style={{fontSize:35, fontWeight:'bold'}}> {activityLevel}% </Text>
            </CardItem>

            <CardItem style={{justifyContent:'center', alignItems:'center'}}>
                <Progress.Bar progress={activityLevel/100} width={300} />
            </CardItem>

          </Card>

          <Card>
            <CardItem header bordered>
              <Text>Menu</Text>
            </CardItem>

            <CardItem>
              <Content>
                  <View style={{flex: 1,flexDirection: 'column'}}>
                    <View style={{flex: 1,}}>
                        <Button block bordered small onPress={() => this.onPressMenu(restaurant)}>
                          <Text>Click for Menu</Text>
                        </Button>
                    </View>
                  </View> 
              </Content>
            </CardItem>
          </Card>

          <Spacer size={20} />
        </Content>
      </Container>
    );
  }
}


RecipeView.propTypes = {
  error: PropTypes.string,
  recipeId: PropTypes.string.isRequired,
  recipes: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

RecipeView.defaultProps = {
  error: null,
};

export default RecipeView;


