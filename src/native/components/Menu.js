import React from 'react';
import PropTypes from 'prop-types';
import { Image, StatusBar } from 'react-native';
import { Container, Content, Card, CardItem, View, Button, Body, H3, List, ListItem, Text, H2} from 'native-base';
import ErrorMessages from '../../constants/errors';
import Error from './Error';
import { Actions } from 'react-native-router-flux';
import Spacer from './Spacer';
import * as Progress from 'react-native-progress';

const MenuView = ({
  error,
  recipes,
  recipeId,
}) => {
  // Error
  if (error) return <Error content={error} />;

  // Get this Recipe from all recipes
  let restaurant = null;
  if (recipeId && recipes) {
    restaurant = recipes.find(item => parseInt(item.id, 10) === parseInt(recipeId, 10));
  }

  // restaurant not found
  if (!restaurant) return <Error content={ErrorMessages.recipe404} />;

  // Build Ingredients listing
  const menu = restaurant.menu.map(item => (
    <ListItem key={item} rightIcon={{ style: { opacity: 0 } }}>
      <Text>{item}</Text>
    </ListItem>
  ));

  // Build Method listing
  const method = restaurant.method.map(item => (
    <ListItem key={item} rightIcon={{ style: { opacity: 0 } }}>
      <Text>{item}</Text>
    </ListItem>
  ));


const keyExtractor = item => item.id;
const onPressBusyness = item => Actions.recipe({ match: { params: { id: String(item.id) } } });
const onPressMenu = item => Actions.menu({ match: { params: { id: String(item.id) } } });

  return (
    <Container>
      <Content padder>
        <Image source={{ uri: restaurant.image }} style={{ height: 150, width: null, flex: 1, borderRadius:10 }} />
        <Spacer size={25} />
        <H3>{restaurant.title}</H3>
        <Text>{restaurant.address}</Text>
        <Spacer size={15} />

        <Card>
          <CardItem header bordered>
            <Text>Menu</Text>
          </CardItem>

          <CardItem>
            <Content>
                  <View style={{flex: 1,}}>
                      <Content>
                        <List>
                          {menu}
                        </List>
                      </Content>
                  </View>
             </Content>
          </CardItem>
        </Card> 

      <Card>  
        <CardItem>
            <Content>
                <View style={{flex: 1,flexDirection: 'column'}}>
                  <View style={{flex: 1,}}>
                      <Button block bordered small onPress={() => onPressBusyness(restaurant)}>
                        <Text>Click for Busyness</Text>
                      </Button>
                  </View>
                </View>  
             </Content>
        </CardItem>          
      </Card>


        <Spacer size={20} />

      </Content>
    </Container>


      
    
  );
};

MenuView.propTypes = {
  error: PropTypes.string,
  recipeId: PropTypes.string.isRequired,
  recipes: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

MenuView.defaultProps = {
  error: null,
};

export default MenuView;
