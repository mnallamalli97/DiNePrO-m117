import React from 'react';
import PropTypes from 'prop-types';
import { Font } from 'expo';
import { FlatList, TouchableOpacity, TouchableHighlight, RefreshControl, Image } from 'react-native';
import { Container, Content, Card, CardItem, Body, Text, Button, StyleSheet, View, H1, H2, H3 } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Loading from './Loading';
import Error from './Error';
import Header from './Header';
import Spacer from './Spacer';
import MapView from 'react-native-maps';
import SearchBar from 'react-native-material-design-searchbar';
import * as Progress from 'react-native-progress';
import { Fonts } from '/Users/meharnallamalli/Desktop/DinePro/src/native/constants/Fonts';

const fontFamily = {
  Elianto: "Elianto",
}

const keyExtractor = item => item.id;
const onPressBusyness = item => Actions.recipe({ match: { params: { id: String(item.id) } } });
const onPressMenu = item => Actions.menu({ match: { params: { id: String(item.id) } } });


const DiningHalls = ({
  error,
  loading,
  ucla,
  reFetch,
}) => {
  // Loading
  if (loading) return <Loading />; 
  if (error) return <Error content={error} />; 

  

  // Content of page

  return (
    <Container>
        
      <Content padder>
        <Header style={{marginBottom: 100, fontFamily: 'Elianto'}}
          title="Welcome to DinePro 1.0"
          content ="Implemented using Heroku backend for retreiving and storing restaurant activity level, Firebase for unstructured data, and the mobile application was built in a cross platform eviornment using React Native"
        />

        <FlatList
          numColumns={1}
          data={ucla}
          renderItem={({ item }) => (
          <Card transparent style={{ paddingHorizontal: 0 }}>
            <CardItem cardBody>
              <TouchableHighlight onPressBusyness={() => onPress(item)} style={{ flex: 1}} underlayColor="#432ECE">
                <Image
                    source={{ uri: item.image }} 
                    style={{
                      justifyContent:'center',
                      alignItems:'center',
                      height: 300,
                      width: null,
                      flex: 1,
                      borderRadius: 10,
                      backgroundColor:'rgba(47, 163, 218, 0.4)' 
                    }} 
                  >

                  <View style={{borderTopWidth:60, borderBottomWidth:15, borderColor: 'transparent'}}>
                    <Text style={{fontSize: 30, color:'white', textAlign:'center', justifyContent:'center', }}> {item.title} </Text>
                    <Text style={{fontSize: 20, color:'white', textAlign:'center', justifyContent:'center',}}> {item.address} </Text>
                    <Text style={{fontSize: 30, color:'white', textAlign:'center', justifyContent:'center',}}>{item.hours}</Text>
                  </View>
                 
                  
                  <Spacer size={5} />

                  <View>
                    <H2 style={{fontSize: 30, color:'white', borderBottomWidth:5, textAlign:'center', justifyContent:'center', }}>Activity Level: {}% </H2>
                    <Progress.Bar progress={value/100} width={300} />
                  </View>



                  <View style={{flex: 1,flexDirection: 'row',alignItems: 'center', justifyContent:'space-between', padding:25}}>
                    <View style={{flex: 1, justifyContent:'space-between', padding:10}}>
                      <Button block bordered small onPressMenu={() => onPress(item)}> 
                        <Text>Menu</Text>
                      </Button>
                    </View>
                     
                    <View style={{flex: 1, }}>
                      <Button block bordered small onPressBusyness={() => onPress(item)}>
                        <Text>Busyness</Text>
                      </Button>
                    </View>
                  </View>


                </Image>
              </TouchableHighlight>
            </CardItem>
          </Card>
          )}
          keyExtractor={keyExtractor}
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={reFetch}
            />
          }
        />

        <Spacer size={20} />
      </Content>
    </Container>
  );
};

DiningHalls.propTypes = {
  error: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  ucla: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  reFetch: PropTypes.func,
};

DiningHalls.defaultProps = {
  error: null,
  reFetch: null,
};




export default DiningHalls;
