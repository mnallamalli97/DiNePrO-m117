export default (resID) => {
	let restaurantLoc;

	switch(resID) {
      case 1:
        restaurantLoc = 'Bella%20Pita%20UCLA';
        break;
      case 2:
        restaurantLoc = 'In-N-Out%20Burger';
        break;
      case 3:
        restaurantLoc = 'Tommy%20Taco';
        break;
      case 4:
        restaurantLoc = 'Fat%20Sal%27s%20Deli';
        break;
      case 5:
        restaurantLoc = 'Bollywood%20Bites';
        break;
      case 6:
        restaurantLoc = 'CAVA';
        break;
      case 7:
        restaurantLoc = 'Chipotle%20Mexican%20Grill';
        break;
      case 8:
        restaurantLoc = 'Falafel%20King%20Inc';
        break;
      case 9:
        restaurantLoc = 'Fatburger';
        break;  
      case 10: 
        restaurantLoc = 'The%20Boiling%20Crab'; 
        break; 
      default:
        restaurantLoc = '';
    }

    return restaurantLoc;
}